﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Test
    {
        static void Main(string[] args)
        {
            //string auth = "server=hexagone.ddns.net;user=users_database;database=users_database;password=1k2o3s4t5y6A;";

            //MySqlConnection conn = new MySqlConnection(auth);
            //conn.Open();

            //MySqlCommand com = new MySqlCommand("show tables;", conn);

            //MySqlDataReader reader = com.ExecuteReader();

            //while(reader.Read())
            //{
            //    Console.WriteLine(reader[0].ToString());
            //}
            //reader.Close();
            //conn.Close();


            //Console.Write("Enter a password: ");
            //string password = Console.ReadLine();

            //// generate a 128-bit salt using a secure PRNG
            //byte[] salt = new byte[128 / 8];
            //using (var rng = RandomNumberGenerator.Create())
            //{
            //    rng.GetBytes(salt);
            //}
            //Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

            //salt = Convert.FromBase64String("yDARAgw9k8O0GPK568yQvw==");

            //// derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            //string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            //    password: password,
            //    salt: salt,
            //    prf: KeyDerivationPrf.HMACSHA1,
            //    iterationCount: 10000,
            //    numBytesRequested: 256 / 8));
            //Console.WriteLine($"Hashed: {hashed}");

            Console.Write("Enter a password: ");
            string password = Console.ReadLine();

            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            Console.WriteLine($"Hashed: {hashed}");

        }
    }
}
