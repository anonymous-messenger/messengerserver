﻿using Anonymous_messenger_conserver.data;
using Anonymous_messenger_conserver.model;
using Anonymous_messenger_conserver.model.msgbody;
using Anonymous_messenger_conserver.service;
using Anonymous_messenger_conserver.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver
{
    class Processor
    {
        

        public static void doProcessing(object o)
        {
            ProcObj obj = o as ProcObj;
            Message m = obj.Message;
            User u = obj.User;

            if (m.Command == Command.DISCONNECT)
            {

                Logger.log(MsgType.INFO, m.Command);
                AuthorizedUsers.getInst().Users.Remove(u.userName);
                return;
            }

            if (m.Command == Command.GET_USER_INFO)
            {

                Logger.log(MsgType.INFO, m.Command);
                UserInfoBody b = new UserInfoBody();
                b.Age = u.age;
                b.Gender = u.gender;
                b.Name = u.userName;
                b.Score = u.score;
                
                m.Body = b;

                SocketIO.SendAll(u.socket, m);

                return;
            }

            if (m.Command == Command.GET_CHANNELS)
            {

                Logger.log(MsgType.INFO, m.Command);
                m.Body = SQL.getChannelsList(u);

                SocketIO.SendAll(u.socket, m);

                return;
            }

            if (m.Command == Command.RAND_CHAT_REQUEST)
            {
                Logger.log(MsgType.INFO, m.Command);
                RandChatRequestBody b = m.Body as RandChatRequestBody;
                if (b.Abort)
                {
                    for(int i = 0; i < RandChatQueue.getInst().List.Count; ++i)
                    {
                        QueueItem item = RandChatQueue.getInst().List[i] as QueueItem;
                        if (u.userName == item.User.userName)
                        {
                            RandChatQueue.getInst().List.RemoveAt(i);
                        }
                    }
                } else
                {
                    RandChatQueue.getInst().List.Add(new QueueItem(u, b));
                }

                
            }

            if (m.Command == Command.SEND_ANONYMOUS_MSG)
            {
                Logger.log(MsgType.INFO, m.Command);
                MsgContentBody b = m.Body as MsgContentBody;
                
                PChannel channel = PrivateChannels.getInst().Channels[b.PrivateChannelId] as PChannel;
                User u2 = channel.getLinkedUser(u);
                SocketIO.SendAll(u2.socket, m);
            }

            if (m.Command == Command.LEAVE_CHAT)
            {

                Logger.log(MsgType.INFO, m.Command);
                MsgContentBody b = m.Body as MsgContentBody;
                PChannel channel = PrivateChannels.getInst().Channels[b.PrivateChannelId] as PChannel;
                SocketIO.SendAll(channel.getLinkedUser(u).socket, m);

                PrivateChannels.getInst().Channels.Remove(b.PrivateChannelId);
            }

            if(m.Command == Command.SET_USER_MARK)
            {
                Logger.log(MsgType.INFO, m.Command);

                SetMarkRequestBody b = m.Body as SetMarkRequestBody;
                PChannel channel = PrivateChannels.getInst().Channels[b.PChannelId] as PChannel;
                User linked = channel.getLinkedUser(u);
                linked.score += b.Count;
                SQL.updateUserMark(linked);
                SocketIO.SendAll(linked.socket, m);
            }

            if(m.Command == Command.REQUEST_FOR_FRIENDSHIP)
            {
                FriendRequestBody b = m.Body as FriendRequestBody;
                PChannel channel = PrivateChannels.getInst().Channels[b.FPChannelId] as PChannel;
                SocketIO.SendAll(channel.getLinkedUser(u).socket, m);
            }

            if(m.Command == Command.ALLLOW_FRIENDSHIP)
            {
                FriendRequestBody b = m.Body as FriendRequestBody;
                PChannel channel = PrivateChannels.getInst().Channels[b.FPChannelId] as PChannel;
                SocketIO.SendAll(channel.getLinkedUser(u).socket, m);
                SQL.createFriendShip(channel);
            }

            if (m.Command == Command.DENY_FRIENDSHIP)
            {
                FriendRequestBody b = m.Body as FriendRequestBody;
                PChannel channel = PrivateChannels.getInst().Channels[b.FPChannelId] as PChannel;
                SocketIO.SendAll(channel.getLinkedUser(u).socket, m);
            }

            if (m.Command == Command.NO_MSG)
            {
                SocketIO.SendAll(u.socket, m);
            }
        }


    }
}
