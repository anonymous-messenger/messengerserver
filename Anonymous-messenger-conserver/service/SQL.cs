﻿using Anonymous_messenger_conserver.data;
using Anonymous_messenger_conserver.model;
using Anonymous_messenger_conserver.model.msgbody;
using Anonymous_messenger_conserver.utils;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.service
{
    class SQL
    {
        public static AuthRegAnswerBody authUser(AuthRegBody body)
        {
            AuthRegAnswerBody ans = new AuthRegAnswerBody();

            MySqlDataReader r = MySQLConnection.getInst().execute("call checkUserName(\'" + body.UserName + "\');");
            while (r.Read())
            {
                if (r[0].ToString() == "1")
                {
                    r.Close();
                    r = MySQLConnection.getInst().execute("call getPassHashData(\'" + body.UserName + "\');");
                    while (r.Read())
                    {
                        

                        string passHash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                            password: body.Pass,
                            salt: Convert.FromBase64String(r[0].ToString()),
                            prf: (KeyDerivationPrf) Enum.Parse(typeof(KeyDerivationPrf), r[2].ToString()),
                            iterationCount: Convert.ToInt32(r[1].ToString()),
                            numBytesRequested: Convert.ToInt32(r[3].ToString())));
                        Logger.log(MsgType.INFO, passHash);
                        r.Close();
                        r = MySQLConnection.getInst().execute("call auth(\'" + body.UserName + "\',\'" + passHash + "\');");

                        while(r.Read())
                        {
                            if (r[0].ToString() == "1")
                            {
                                ans.Success = true;
                                ans.Desc = "Access allowed";
                            } else
                            {
                                ans.Success = false;
                                ans.Desc = "Access denied";
                            }
                            break;
                        }

                        break;
                    }
                    break;
                } else
                {
                    ans.Success = false;
                    ans.Desc = "User does not exist!";

                }
            }
            r.Close();
            return ans;
        } 



        public static User buildUserAfterAuth(AuthRegBody body)
        {
            User u = new User();
            u.userName = body.UserName;

            MySqlDataReader r = MySQLConnection.getInst().execute("call getUserInfo(\'" + body.UserName + "\');");
            while (r.Read())
            {
                u.age = Convert.ToInt32(r[0].ToString());
                u.gender = r[1].ToString();
                u.score = Convert.ToInt32(r[2].ToString());
                break;
            }
            r.Close();
            return u;
        } 


        public static AuthRegAnswerBody registerUser(AuthRegBody body)
        {
            AuthRegAnswerBody ans = new AuthRegAnswerBody();

            MySqlDataReader r = MySQLConnection.getInst().execute("call checkUserName(" + body.UserName + ");");

            while (r.Read())
            {
                if (r[0].ToString() == "1")
                {
                    ans.Success = false;
                    ans.Desc = "This user already exists";
                    
                } else
                {
                    Logger.log(MsgType.INFO, "Registration...");

                    byte[] salt = new byte[128 / 8];
                    using (var rng = RandomNumberGenerator.Create())
                    {
                        rng.GetBytes(salt);
                    }
                    

                    string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                        password: body.Pass,
                        salt: salt,
                        prf: KeyDerivationPrf.HMACSHA1,
                        iterationCount: 10000,
                        numBytesRequested: 256 / 8));

                    r.Close();
                    r = MySQLConnection.getInst().execute("call register(\'" 
                        + body.UserName + "\',\'" 
                        + body.Age.ToString() + "\',\'"
                        + body.Gender + "\',\'"
                        + hashed + "\',\'"
                        + Convert.ToBase64String(salt) + "\',\'"
                        + "10000" + "\',\'"
                        + Convert.ToInt32(KeyDerivationPrf.HMACSHA1).ToString() + "\',\'"
                        + "32" + "\');");

                    while (r.Read())
                    {
                        if (r[0].ToString() == "1")
                        {
                            ans.Success = true;
                            ans.Desc = "Registered";

                        } else
                        {
                            ans.Success = false;
                            ans.Desc = "Can\'t register";
                        }

                        break;
                    }
                }
                break;
            }
            r.Close();
            return ans;
        }


        public static void createFriendShip(PChannel channel)
        {
            MySqlDataReader r = MySQLConnection.getInst().execute("call createFriendShip(\'" + channel.User1.userName + "\',\'" + channel.User2.userName + "\');");
            r.Close();

        }

        public static void updateUserMark(User user)
        {
            MySqlDataReader r = MySQLConnection.getInst().execute("call setUserMark(\'" + user.userName + "\',\'" + user.score.ToString() + "\');");
            r.Close();
        }

        public static ChannelsListBody getChannelsList(User user)
        {
            ChannelsListBody b = new ChannelsListBody();
            MySqlDataReader r = MySQLConnection.getInst().execute("call getUserChannels(\'" + user.userName + "\');");

            while (r.Read())
            {
                Channel c = new Channel();
                c.Id = Convert.ToInt32(r[0].ToString());
                c.Name = r[1].ToString();
                b.Channels.Add(c);

            }
            r.Close();
            return b;
        }
    }
}
