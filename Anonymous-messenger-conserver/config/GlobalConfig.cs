﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.config
{
    class GlobalConfig
    {
        public static readonly string MYSQL_USER = "server=hexagone.ddns.net;user=users_database;database=users_database;password=1k2o3s4t5y6A;";
        public static readonly IPAddress HOST_IP = new IPAddress(new byte[] { 192, 168, 1, 48 });
        public static readonly int SERVER_PORT = 25565;
        public static readonly IPEndPoint END_POINT = new IPEndPoint(HOST_IP, SERVER_PORT);

    }
}
