﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model
{
    class Command
    {
        public static readonly string NO_MSG = "NO_MSG";
        public static readonly string AUTH = "AUTH";
        public static readonly string AUTH_ANSWER = "AUTH_ANSWER";
        public static readonly string REGISTER = "REGISTER";
        public static readonly string DISCONNECT = "DISCONNECT";

        public static readonly string SEND_ANONYMOUS_MSG = "ANONYMOUS_MSG";
        public static readonly string SEND_PERSONAL_MSG = "PERSONAL_MSG";
        public static readonly string SEND_GROUP_MSG = "GROUP_MSG";

        public static readonly string RAND_CHAT_REQUEST = "RAND_CHAT_REQUEST";

        public static readonly string REPLY_TO_ANONYMOUS_MSG = "REPLY_ANONYMOUS_MSG";
        public static readonly string REPLY_TO_PERSONAL_MSG = "REPLY_PERSONAL_MSG";
        public static readonly string REPLY_TO_GROUP_MSG = "REPLY_GROUP_MSG";


        public static readonly string REQUEST_FOR_FRIENDSHIP = "REQUEST_FOR_FRIENDSHIP";
        public static readonly string ALLLOW_FRIENDSHIP = "ALLOW_FRIENDSHIP";
        public static readonly string DENY_FRIENDSHIP = "DENY_FRIENDSHIP";

        public static readonly string SET_USER_MARK = "SET_USER_MARK";

        public static readonly string LEAVE_CHAT = "LEAVE_CHAT";

        public static readonly string GET_CATEGORIES = "GET_CATEGORIES";
        public static readonly string GET_MESSAGES = "GET_MESSAGES";
        public static readonly string GET_CHANNELS = "GET_CHANNELS";
        public static readonly string GET_USER_INFO = "GET_USER_INFO";


    }
}
