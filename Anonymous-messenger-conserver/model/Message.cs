﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model
{
    class Message
    {
        public string Command { get; set; }
        public msgbody.MsgBody Body { get; set; }
    }
}
