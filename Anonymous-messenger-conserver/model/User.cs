﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model
{
    class User
    {
        public Socket socket
        {
            get; set;
        }

        public int age { get; set; }
        public int score { get; set; }
        public string gender { get; set; }
        public string userName
        {
            get; set;
        }
    }
}
