﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{
    class MsgContentBody : MsgBody
    {
        public string Time { get; set; }
        public string From { get; set; }
        public string Text { get; set; }
        public int ChannelId { get; set; }
        public int PrivateChannelId { get; set; }

    }
}
