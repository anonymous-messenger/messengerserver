﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{
    class AuthRegAnswerBody : MsgBody
    {
        public bool Success { get; set; }
        public string Desc { get; set; }
    }
}
