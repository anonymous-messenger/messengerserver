﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{
    class MessagesListBody : MsgBody
    {
        public List<MsgContentBody> Messages { get; set; } = new List<MsgContentBody>();

    }
}
