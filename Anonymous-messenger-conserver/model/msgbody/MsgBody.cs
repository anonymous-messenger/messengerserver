﻿using JsonSubTypes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{

    [JsonConverter(typeof(JsonSubtypes))]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(AuthRegBody), "Pass")]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(AuthRegAnswerBody), "Success")]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(MsgContentBody), "Text")]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(UserInfoBody), "Score")]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(ChannelsListBody), "Channels")]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(MessagesListBody), "Messages")]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(RandChatRequestBody), "Abort")]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(FriendRequestBody), "FPChannelId")]
    [JsonSubtypes.KnownSubTypeWithProperty(typeof(SetMarkRequestBody), "Count")]
    class MsgBody
    {

    }
}
