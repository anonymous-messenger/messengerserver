﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{
    class UserInfoBody : MsgBody
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public int Score { get; set; }

    }
}
