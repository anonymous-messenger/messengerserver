﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{
    class AuthRegBody : MsgBody
    {
        public string UserName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string Pass { get; set; }

        public AuthRegBody(string name, string gender, int age, string pass)
        {
            this.UserName = name; this.Gender = gender; this.Age = age; this.Pass = pass;
        }

    }
}
