﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{
    class SetMarkRequestBody : MsgBody
    {
        public string UserTo { get; set; }
        public int PChannelId { get; set; }
        public int Count { get; set; }
    }
}
