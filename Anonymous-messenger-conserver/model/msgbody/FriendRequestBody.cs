﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{
    class FriendRequestBody : MsgBody
    {
        public bool Allow { get; set; }
        public string UserFrom { get; set; }
        public string UserTo { get; set; }
        public int FPChannelId { get; set; }

    }
}
