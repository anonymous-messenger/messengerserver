﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.model.msgbody
{
    class Channel
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "default";
    }

    class ChannelsListBody : MsgBody
    {
        public List<Channel> Channels { get; set; } = new List<Channel>();
    }
}
