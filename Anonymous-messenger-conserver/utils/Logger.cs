﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.utils
{
    class MsgType
    {
        public static readonly string ERROR = "ERR";
        public static readonly string INFO = "INF";

    } 

    class Logger
    {


        private static object sync = new object();

        public static void log(String type, String msg)
        {
            lock (sync)
            {
                Console.WriteLine("[" + type + "]=[" + DateTime.Now.ToString() + "]=: " + msg);
            }
        }

    }
}
