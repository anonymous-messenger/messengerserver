﻿using Anonymous_messenger_conserver.model;
using System.Runtime.Serialization.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Anonymous_messenger_conserver.model.msgbody;

namespace Anonymous_messenger_conserver.utils
{

    class SocketIO
    {
        private static string endSuffix = "###END";

        public static List<Message> ReceiveAllList(Socket socket)
        {
            if (!IsSocketStillConnected(socket)) return null;

            string tmpstr = String.Empty;
            byte[] buffer = new byte[1024];
            Int32 read = 0;


            while (true)
            {
                try
                {
                    read = socket.Receive(buffer);
                }
                catch (Exception e)
                {
                    Logger.log(MsgType.INFO, e.StackTrace);
                    break;
                }
                tmpstr += Encoding.UTF8.GetString(buffer, 0, read);

                if (tmpstr.EndsWith(endSuffix)) break;
            }
            Logger.log(MsgType.INFO, tmpstr);
            if (tmpstr.Length < endSuffix.Length) return null;

            List<Message> m = new List<Message>();
            var a = tmpstr.Split(new[] { endSuffix, }, StringSplitOptions.RemoveEmptyEntries);
            foreach (String t in a)
            {
                m.Add(JsonConvert.DeserializeObject<Message>(t, new JsonSerializerSettings
                {
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc
                }));
            }


            return m;
        }

        public static List<Message> ReceiveAllList(User user)
        {
            return ReceiveAllList(user.socket);
        }


        public static Message ReceiveAll(Socket socket)
        {
            if (!IsSocketStillConnected(socket)) return null;

            string tmpstr = String.Empty;
            byte[] buffer = new byte[1024];
            Int32 read = 0;


            while (true)
            {
                try
                {
                    read = socket.Receive(buffer);
                }
                catch (Exception e)
                {
                    Logger.log(MsgType.INFO, e.StackTrace);
                    break;
                }
                tmpstr += Encoding.UTF8.GetString(buffer, 0, read);

                if (tmpstr.EndsWith(endSuffix)) break;
            }
            if (tmpstr.Length > endSuffix.Length)
                tmpstr = tmpstr.Remove(tmpstr.Length - endSuffix.Length);

            if (tmpstr.Length < endSuffix.Length) return null;
            Logger.log(MsgType.INFO, tmpstr);

            return JsonConvert.DeserializeObject<Message>(tmpstr, new JsonSerializerSettings
            {
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            });
        }

        public static Message ReceiveAll(User user)
        {
            return ReceiveAll(user.socket);
        }

        public static void SendAll(User user, Message obj)
        {
            SendAll(user.socket, obj);
        }

        public static void SendAll(Socket socket, Message obj)
        {
            if (!IsSocketStillConnected(socket)) return;
            string msg = JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            });
            Logger.log(MsgType.INFO, msg);

            msg += endSuffix;
            byte[] msgb = Encoding.UTF8.GetBytes(msg);

            try
            {
                Logger.log(MsgType.INFO, socket.Send(msgb, msgb.Length, 0).ToString() + " bytes sended.");
                Logger.log(MsgType.INFO, msgb.Length.ToString() + " msg length in bytes");
            }
            catch (Exception e)
            {
                Logger.log(MsgType.INFO, e.StackTrace);
            }
        }

        public static bool IsSocketStillConnected(Socket socket)
        {
            bool connected = true;
            bool blockingState = socket.Blocking;
            try
            {
                byte[] tmp = new byte[1];
                socket.Blocking = false;
                socket.Send(tmp, 0, 0);
            }
            catch (SocketException e)
            {

                connected = false;
                Logger.log(MsgType.INFO, e.StackTrace);
            }
            finally
            {
                socket.Blocking = blockingState;
            }
            return connected;
        }
    }
}
