﻿using Anonymous_messenger_conserver.data;
using Anonymous_messenger_conserver.model;
using Anonymous_messenger_conserver.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver
{
    class ListenerThread
    {
        public bool Closed { get; set; } = false;
        public void listener()
        {
            Logger.log(MsgType.INFO, "Msg listening...");
            while (!Closed)
            {
                try
                {
                    foreach (User user in AuthorizedUsers.getInst().Users.Values)
                    {
                        if (!SocketIO.IsSocketStillConnected(user.socket))
                        {
                            AuthorizedUsers.getInst().Users.Remove(user.userName);
                        }

                        if (user.socket.Available > 0)
                        {
                            List<Message> msgs = SocketIO.ReceiveAllList(user.socket);

                            foreach (Message m in msgs)
                            {
                                ProcObj obj = new ProcObj();
                                obj.Message = m;
                                obj.User = user;

                                ThreadPool.QueueUserWorkItem(Processor.doProcessing, obj);
                            }
                            //Messages.getInst().Msgs.Enqueue(m);
                        }
                        Thread.Sleep(100 / (AuthorizedUsers.getInst().Users.Count + 1));
                    }

                } catch {
                    Logger.log(MsgType.ERROR, "Exception in Listener Thread");
                }
                
            }
        }
    }
}
