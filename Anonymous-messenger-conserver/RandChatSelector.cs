﻿using Anonymous_messenger_conserver.data;
using Anonymous_messenger_conserver.model;
using Anonymous_messenger_conserver.model.msgbody;
using Anonymous_messenger_conserver.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver
{
    class RandChatSelector
    {

        public bool Closed { get; set; } = false;

        public void selector()
        {
            Logger.log(MsgType.INFO, "Processing random selection...");
            while (!Closed)
            {
                try
                {
                    if(RandChatQueue.getInst().List.Count > 0)
                    {
                    
                        for(int i = 0; i < RandChatQueue.getInst().List.Count; ++i)
                        {
                            for (int ii = 0; ii < RandChatQueue.getInst().List.Count; ++ii)
                            {
                                if (i != ii)
                                {
                                    QueueItem i1 = RandChatQueue.getInst().List[i] as QueueItem;
                                    QueueItem i2 = RandChatQueue.getInst().List[ii] as QueueItem;

                                    if (i1.User.age < i2.Request.MaxAge
                                        && i1.User.age > i2.Request.MinAge
                                        && i2.User.age < i1.Request.MaxAge
                                        && i2.User.age > i1.Request.MinAge)
                                    {
                                        if ((i1.Request.Gender == "none" && i2.Request.Gender == "none")
                                            || (i1.User.gender == i2.Request.Gender && i2.User.gender == i1.Request.Gender)
                                            || (i1.User.gender == i2.Request.Gender && i1.Request.Gender == "none")
                                            || (i2.User.gender == i1.Request.Gender && i2.Request.Gender == "none"))
                                        {
                                            RandChatRequestBody ans = new RandChatRequestBody();
                                            ans.PChannelId = PrivateChannels.getInst().addChannel(i1.User, i2.User);
                                            ans.Abort = false;
                                            Message m = new Message();
                                            m.Command = Command.RAND_CHAT_REQUEST;
                                            m.Body = ans;
                                            RandChatQueue.getInst().List.RemoveAt(i);
                                            RandChatQueue.getInst().List.RemoveAt(ii-1);
                                            SocketIO.SendAll(i1.User.socket, m);
                                            SocketIO.SendAll(i2.User.socket, m);
                                        }
                                    }
                                }
                            }
                        }

                    }

                    Thread.Sleep(1000 / (RandChatQueue.getInst().List.Count + 1));
                }
                catch
                {
                    Logger.log(MsgType.INFO, "Exception in Selector Thread");
                }

            }
        }


    }
}
