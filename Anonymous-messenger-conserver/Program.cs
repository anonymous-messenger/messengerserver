﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Anonymous_messenger_conserver.config;
using Anonymous_messenger_conserver.data;
using Anonymous_messenger_conserver.model;
using Anonymous_messenger_conserver.model.msgbody;
using Anonymous_messenger_conserver.service;
using Anonymous_messenger_conserver.utils;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Anonymous_messenger_conserver
{
    class Program
    {
        public static Socket listener;
        public static object sync = new object();
        private static bool closed = false;
        public static ListenerThread ListenerThread = new ListenerThread();
        public static RandChatSelector SelectorThread = new RandChatSelector();

        static void Main(string[] args)
        {
            Thread cmdListener = new Thread(CmdListener);
            cmdListener.Start();
            Thread msgListener = new Thread(ListenerThread.listener);
            msgListener.Start();
            Thread selectorThread = new Thread(SelectorThread.selector);
            selectorThread.Start();


            listener = new Socket(GlobalConfig.END_POINT.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(GlobalConfig.END_POINT);
            listener.Listen(1000);

            while(!closed)
            {
                lock(sync)
                {
                    Socket newClient = listener.Accept();

                    Logger.log(MsgType.INFO, "Accepting...");

                    Message msg = SocketIO.ReceiveAll(newClient);
                    
                    if (msg.Command == Command.AUTH)
                    {
                        AuthRegBody b = msg.Body as AuthRegBody;
                        AuthRegAnswerBody ans = SQL.authUser(b);
                        msg.Command = Command.AUTH_ANSWER;
                        msg.Body = ans;
                        SocketIO.SendAll(newClient, msg);

                        if (ans.Success)
                        {
                            User u = SQL.buildUserAfterAuth(b);
                            u.socket = newClient;
                            AuthorizedUsers.getInst().addUser(u);
                        } else
                        {
                            Logger.log(MsgType.INFO, ans.Desc);
                        }
                    }

                    if (msg.Command == Command.REGISTER)
                    {
                        AuthRegBody b = msg.Body as AuthRegBody;
                        AuthRegAnswerBody ans = SQL.registerUser(b);
                        msg.Command = Command.AUTH_ANSWER;
                        msg.Body = ans;
                        SocketIO.SendAll(newClient, msg);

                        if (ans.Success)
                        {
                            User u = SQL.buildUserAfterAuth(b);
                            u.socket = newClient;
                            AuthorizedUsers.getInst().addUser(u);
                        }
                        else
                        {
                            Logger.log(MsgType.INFO, ans.Desc);
                        }

                    }
                }
            }
        }


        public static void CmdListener()
        {
            while (true)
            {
                string input = Console.ReadLine();
                if (input == "/stop")
                {
                    Logger.log(MsgType.INFO, "Stopping... Press any key...");
                    closed = true;
                    ListenerThread.Closed = true;
                    SelectorThread.Closed = true;
                    lock(sync)
                    {
                        listener.Close();
                    }
                    Console.ReadKey();
                    Environment.Exit(0);

                }
            }

        }
    }
}
