﻿using Anonymous_messenger_conserver.model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.data
{
    class PChannel
    {
        public User User1 { get; set; } 
        public User User2 { get; set; }

        public int ChannelId { get; set; }

        public PChannel(User u1, User u2, int id)
        {
            this.User1 = u1;
            this.User2 = u2;
            this.ChannelId = id;
        }

        public User getLinkedUser(User another)
        {
            if (another.userName != User1.userName)
            {
                return User1;
            } else
            {
                return User2;
            }
        }
    }

    class PrivateChannels
    {
        public static int lastChatId = 0;
        private static PrivateChannels privateChannels;
        public Hashtable Channels { get; } = Hashtable.Synchronized(new Hashtable());

        public static PrivateChannels getInst()
        {
            if(privateChannels == null)
            {
                privateChannels = new PrivateChannels();
            }
            return privateChannels;
        }


        public int addChannel(User u1, User u2)
        {
            lastChatId++;
            Channels.Add(lastChatId, new PChannel(u1, u2, lastChatId));
            return lastChatId;
        }
    }
}
