﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections;
using System.Threading.Tasks;
using Anonymous_messenger_conserver.model.msgbody;
using Anonymous_messenger_conserver.model;

namespace Anonymous_messenger_conserver.data
{
    class QueueItem
    {
        public User User { get; set; }
        public RandChatRequestBody Request { get; set; }

        public QueueItem(User u, RandChatRequestBody b)
        {
            this.User = u;
            this.Request = b;
        }

    }

    class RandChatQueue
    {
        private static RandChatQueue randChatQueue;
        public static RandChatQueue getInst()
        {
            if (randChatQueue == null)
            {
                randChatQueue = new RandChatQueue();
            }
            return randChatQueue;
        }

        public ArrayList List { get; } = ArrayList.Synchronized(new ArrayList());
    }
}
