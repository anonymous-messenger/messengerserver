﻿using Anonymous_messenger_conserver.config;
using Anonymous_messenger_conserver.utils;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.data
{
    class MySQLConnection
    {
        private static MySQLConnection inst;
        private static object sync = new object();

        private MySqlConnection connection = new MySqlConnection(GlobalConfig.MYSQL_USER);

        public static MySQLConnection getInst()
        {

            if (inst == null)
            {
                inst = new MySQLConnection();
                inst.connection.Open();
            }
            return inst;

        }


        public MySqlDataReader execute(string query)
        {
            lock (sync)
            {
                try
                {
                    return new MySqlCommand(query, connection).ExecuteReader();
                } catch (Exception ex)
                {
                    Logger.log(MsgType.ERROR, ex.StackTrace);
                    return new MySqlCommand("SELECT \"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\";", connection).ExecuteReader();
                }
            }
        }


        
    }
}
