﻿using Anonymous_messenger_conserver.model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_messenger_conserver.data
{
    class AuthorizedUsers
    {
        private static AuthorizedUsers inst;
        

        public static AuthorizedUsers getInst()
        {
            if (inst == null) 
            {
                inst = new AuthorizedUsers();
            }
            return inst;
        }


        public Hashtable Users { get; } = Hashtable.Synchronized(new Hashtable());


        private AuthorizedUsers()
        {
            
        }

        public void addUser(User user) 
        {
            if (Users.Contains(user.userName))
            {
                Users.Remove(user.userName);
            }
            Users.Add(user.userName, user);
            
        }

    }
}
